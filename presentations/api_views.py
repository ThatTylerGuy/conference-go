from django.http import JsonResponse
from .models import Presentation
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        attendees = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=PresentationDetailEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            presentation = Presentation.objects.get(id=conference_id)
            content["Presentation"] = presentation
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid presentation id"},
                status=400,
            )

        attendee = Presentation.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


def api_show_presentation(request, id):
    """
    Returns the details for the Presentation model specified
    by the id parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL

    {
        "presenter_name": the name of the presenter,
        "company_name": the name of the presenter's company,
        "presenter_email": the email address of the presenter,
        "title": the title of the presentation,
        "synopsis": the synopsis for the presentation,
        "created": the date/time when the record was created,
        "status": the name of the status for the presentation,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        presentation, encoder=PresentationDetailEncoder, safe=False
    )
